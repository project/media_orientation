<?php

namespace Drupal\media_orientation\Commands;

use Drupal\media\Entity\Media;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class MediaOrientationCommands extends DrushCommands {

  /**
   * General function to resave all media for a specific bundle.
   *
   * @command mo:resave
   *
   * @param string $bundle
   *   Which media bundle to resave all entities for.
   *
   * @usage mo:resave image
   */
  public function migrate($bundle) {
    $this->output()->writeln('=== Start Media orientation resave ====');

    if (empty($bundle)) {
      $this->output()->writeln('Error: required type.');
      return;
    }

    $medias = \Drupal::entityQuery('media')
      ->condition('bundle', $bundle)
      ->accessCheck(FALSE)
      ->execute();

    $loadedMedias = Media::loadMultiple($medias);
    foreach ($loadedMedias as $loadedMedia) {
      $loadedMedia->save();
    }

    $this->output()->writeln('=== DONE ====');
  }

}
