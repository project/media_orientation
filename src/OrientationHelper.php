<?php

namespace Drupal\media_orientation;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file\Entity\File;

/**
 * Orientationhelper enum.
 */
enum OrientationHelper: string {
  case LANDSCAPE = 'landscape';
  case SQUARE = 'square';
  case PORTRAIT = 'portrait';

}

/**
 * Exif Orientation helper class.
 */
class OrientationHelper {

  use StringTranslationTrait;

  /**
   * The orientation enum value.
   *
   * @var \Drupal\media_orientation\OrientationHelper
   */
  private $orientation;

  /**
   * Sets the orientation for a width & height.
   *
   * @param int $width
   *   The width of the image.
   * @param int $height
   *   The Height of the image.
   */
  public function determineOrientation($width, $height) {
    if ($width > $height) {
      $this->orientation = OrientationHelper::LANDSCAPE;
    }
    elseif ($width == $height) {
      $this->orientation = OrientationHelper::SQUARE;
    }
    else {
      $this->orientation = OrientationHelper::PORTRAIT;
    }
  }

  /**
   * Get the label for the current orientation.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The string stating the orientation.
   */
  public function getTextualOrientation() {
    switch ($this->orientation) {
      case OrientationHelper::PORTRAIT:
        return $this->t('Portrait');

      case OrientationHelper::SQUARE:
        return $this->t('Square');

      default:
        return $this->t('Landscape');
    }
  }

  /**
   * Get the orientation.
   *
   * @return \Drupal\media_orientation\OrientationHelper
   *   The orientation enum object.
   */
  public function getOrientation(): OrientationHelper {
    return $this->orientation;
  }

  /**
   * Saves orientation for a media entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The Media entity.
   */
  public function saveMediaEntityOrientation(EntityInterface $entity) {
    $bundle_entity_type = $entity->getEntityType()->getBundleEntityType();
    /** @var \Drupal\Core\Config\Entity\ConfigEntityBase $entity_type */
    $entity_type = \Drupal::entityTypeManager()->getStorage($bundle_entity_type)->load($entity->bundle());

    if ($entity->getSource()->getPluginId() !== 'image') {
      return;
    }

    $orientationFieldName = $entity_type->getThirdPartySettings('media_orientation')['orientation'] ?? NULL;
    if (empty($orientationFieldName)) {
      return;
    }

    $imageSourceField = $entity_type->getSource()->getConfiguration()['source_field'];
    $fileId = $entity->get($imageSourceField)->getValue()[0]['target_id'] ?? NULL;

    if (empty($fileId)) {
      return;
    }

    $fileGenerator = \Drupal::service('file_url_generator');

    $file = File::load($fileId);
    if (!file_exists($fileGenerator->generateAbsoluteString($file->getFileUri()))) {
      return;
    }

    $imageSize = getimagesize($file->getFileUri());

    if ($imageSize !== FALSE) {
      $this->determineOrientation($imageSize[0], $imageSize[1]);
      $entity->set($orientationFieldName, $this->getOrientation()->value);
    }
  }

}
