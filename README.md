# Media Orientation

For media image types it allows you to select a field where the orientation
information about the image is saved.

This information can be used to display the orientation and or to filter upon
this orientation in the media library.

## Table of contents

 - Requirements
 - Installation
 - Configuration
 - Maintainers

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Have a media Image type
1. Install this media and views_filter_select
1. Create a text field on the media type
1. Select the textfield for Media orientation (On the media type configuration
page)
1. Adapt the Media library view for example or the form display to display this
orientation

## Maintainers

Current maintainers:
- [Mschudders](https://www.drupal.org/u/mschudders)
